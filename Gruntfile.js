module.exports = function(grunt) {

    const sass = require('node-sass');

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            options: {
                implementation: sass,
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        /* endSass */
        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },
        /* endWatch */
        browserSync: {
            dev: {
                bsFiles: { //browser files
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    server: {
                        baseDir: './' //Directorio base para nuestro servidor
                    },
                    watchTask: true

                }
            }
        },
        /* endbrowserSync */
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'img/*.{png,gif,jpg,jpeg}',
                    dest: 'dist/'
                }]
            }
        },
        /* endimagemin */
        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/@fortawesome/fontawesome-free', // change this for font-awesome
                    src: ['**/*.*'],
                    dest: 'dist'

                }]
            }

        },
        /* endcopy */
        clean: {
            build: {
                src: ['dist/']
            }
        },
        /* endclean */
        cssmin: {
            dist: {}
        },
        /* endcssmin */
        uglify: {
            dist: {}
        },
        /*enduglify*/
        filerev: {
            option: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release: {
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css'
                    ]
                }]
            }
        },
        /* endfilerev */
        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },
        /* endconcat */
        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 'about.html', 'precios.html', 'contacto.html', 'terminos-cond.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },
        /* enduseminPrepare */
        usemin: {
            html: ['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/contacto.html', 'dist/terminos-cond.html'],
            options: {
                assentsDir: ['dist', 'dist/css', 'dist/js']
            }
        },
        /* endusemin */
    });

    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ]);


};